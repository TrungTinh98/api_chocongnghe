import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:async';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database? _db;

  Future<Database?> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    String path = join(await getDatabasesPath(), "user.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE User(id INTEGER PRIMARY KEY, username TEXT, password TEXT)");
  }

  Future<int?> saveUser(Map<String, dynamic> user) async {
    var dbClient = await db;
    int? res = await dbClient?.insert("User", user);
    return res;
  }

  Future<Map<String, dynamic>?> getUser() async {
    var dbClient = await db;
    List<Map<String, dynamic>>? res = await dbClient?.query("User", limit: 1);
    if (res != null && res.isNotEmpty) {
      return res.first;
    }
    return null;
  }
}
