import 'db_helper.dart';

Future<bool> checkLoginStatus() async {
  DatabaseHelper dbHelper = DatabaseHelper();
  var user = await dbHelper.getUser();
  return user != null;
}
