import 'package:flutter/material.dart';
import '../screens/home_screen.dart';
import '../screens/notify_screen.dart';
import '../screens/profile_screen.dart';

const List<Widget> widgetNavigationBar = <Widget>[
  HomeScreen(),
  NotifyScreen(),
  ProfileScreen(),
];
