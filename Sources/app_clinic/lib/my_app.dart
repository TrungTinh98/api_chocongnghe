import 'dart:io';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import './layouts/desktop_layout.dart';
import './layouts/mobile_layout.dart';
import './layouts/web_layout.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});
  @override
  // ignore: library_private_types_in_public_api
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    if (kIsWeb) {
      return const WebLayout();
    } else if (Platform.isAndroid || Platform.isIOS) {
      return const MobileLayout();
    } else if (Platform.isWindows || Platform.isLinux || Platform.isMacOS) {
      return const DesktopLayout();
    } else {
      return const Scaffold(
        body: Center(
          child: Text('Unsupported platform'),
        ),
      );
    }
  }
}
