import '/utils/menu_helper.dart';
import '/widgets/cus_bottom_na.dart';
import 'package:flutter/material.dart';
import '../utils/auth_helper.dart';
import '../login.dart';

class WebLayout extends StatefulWidget {
  const WebLayout({Key? key}) : super(key: key);

  @override
  _WebLayoutState createState() => _WebLayoutState();
}

class _WebLayoutState extends State<WebLayout> {
  int _selectedIndex = 0;
  bool _isLoading = true;
  bool _isLoggedIn = true;

  @override
  void initState() {
    super.initState();
    _initializeLoginStatus();
  }

  void _initializeLoginStatus() async {
    bool loggedIn = await checkLoginStatus();
    setState(() {
      _isLoggedIn = loggedIn;
      _isLoading = false;
    });
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading) {
      return const MaterialApp(
        home: Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      );
    }
    return MaterialApp(
      title: '',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: _isLoggedIn
          ? const LoginScreen()
          : Scaffold(
              appBar: AppBar(
                title: const Text('My Flutter Web'),
              ),
              body: widgetNavigationBar.elementAt(_selectedIndex),
              bottomNavigationBar: CustomBottomNavigationBar(
                currentIndex: _selectedIndex,
                onItemTapped: _onItemTapped,
              ),
            ),
    );
  }
}
